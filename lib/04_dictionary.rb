class Dictionary
  def initialize; @entries = {}; end
  attr_accessor :entries

  def add(input)
    input.class != Hash ?
      @entries[input]=nil : @entries = @entries.merge(input)
  end

  def keywords
    @entries.keys.sort
  end

  def include?(k)
    @entries.has_key?(k)
  end

  def find(prefix)
    #return a hash with keys that include the prefix.
    @entries.select{|k,v| k.include?(prefix)}.to_h
  end

  def printable
    @printable = @entries.sort.map do |k,v|
        "[#{k}] \"#{v}\""
    end
    @printable.join("\n")
  end
end
