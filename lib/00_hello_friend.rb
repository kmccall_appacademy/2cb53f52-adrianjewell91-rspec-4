class Friend
  
  def initialize; end

  def greeting(who=nil)
    who!=nil ? "Hello, #{who}!" : "Hello!" #The ternary
  end #Remember to go one rspec at a time.
end
