class Book

  def initialize; end
  def title; @title; end

  def title=(title)
    @intermediate = title.split(' ').map do |el|
      case ['a','an','and','in','the','of'].include?(el)
      when true
        el
      when false
        el.capitalize
      end
    end
    @intermediate[0] = @intermediate[0].capitalize
    @title = @intermediate.join(' ')
  end
end
