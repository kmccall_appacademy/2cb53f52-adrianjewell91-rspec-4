class Timer
 def initialize; @seconds = 0; end
 def seconds; @seconds; end

 def seconds=(seconds)
   @seconds = seconds
   @time_string = set_time_string(@seconds)
   @seconds
 end

 def time_string
   @time_string
 end

 private

 def set_time_string(seconds) #need to make it a loop.
  @s_hand = seconds%60
  @min = (seconds%3600 - @s_hand)/60
  @hours = (seconds-(seconds%3600))/3600

  @string = [@hours,@min,@s_hand]
  @string.map{|el| insert(el)}.join(':')
 end

 def insert(var)
   var < 10 ? "0#{var}" : "#{var}"
 end
end

=begins
  @hours < 10 ?
    @string<<"0#{@hours}" : @string<<"#{@hours}"

  @min < 10 ?
    @string<<"0#{@min}" : @string<<"#{@min}"

  @s_hand < 10 ?
    @string<<"0#{@s_hand}" : @string<<"#{@s_hand}"
=end
