class Temperature

  def initialize(options={})
    @options = options
  end

  attr_accessor :options

  def in_fahrenheit
    @options[:f] ? @options[:f] : ctof(@options[:c])
  end

  def in_celsius
    @options[:c] ? @options[:c] : ftoc(@options[:f])
  end

  def self.from_celsius(val) #A special way to handle classes.
    self.new(c: val)
  end

  def self.from_fahrenheit(val)
    self.new(f: val)
  end

  #private

  def ftoc(num)
    (num.to_f - 32)*(5.0/9.0)
  end

  def ctof(num)
    (num.to_f * (9.0/5.0)) + 32
  end

end


class Celsius < Temperature
  def initialize(num)
    @options = {c: num}
  end
end

class Fahrenheit < Temperature
  def initialize(num)
    @options = {f: num}
  end
end
